import cv from "../assets/cv.pdf";
import { nav } from "../data/site.json";

/**
 * @file NavBar.tsx
 * @description NavBar component
 * @date 17/12/2022 - 15:17:53
 * @author Mathieu Piton
 *
 * @returns {JSX.Element}
 */
const NavBar = (): JSX.Element => {
  const openHamburger = () => {
    const nav = document.querySelector(".navbar-collapse");
    if (nav) {
      nav.classList.toggle("show");
    }
  };

  return (
    <nav
      className="navbar navbar-default navbar-fixed-top navbar-expand-lg"
      role="navigation"
    >
      <div className="container-fluid hidden">
        <button
          type="button"
          className="navbar-toggler"
          data-toggle="collapse"
          data-target="#navbarCollapse"
          aria-controls="navbarCollapse"
          aria-expanded="false"
          aria-label="Toggle navigation"
          onClick={openHamburger}
        >
          <span className="navbar-toggler-icon"></span>
        </button>
      </div>
      <div className="navbar-collapse collapse" id="navbarCollapse">
        <ul className="navbar-nav flex-row w-100 justify-content-center">
          {nav.map((item) => {
            if (
              window.location.pathname === "/veille" &&
              item.name === "Veille"
            ) {
              item.name = "Accueil";
              item.link = "/";
              return (
                <li key={item.id} className="nav-item">
                  <a
                    className="nav-link px-3 nav-link-hover active"
                    href={item.link}
                  >
                    <strong>{item.name.toUpperCase()}</strong>
                  </a>
                </li>
              );
            } else {
              return (
                <li key={item.id} className="nav-item">
                  <a className="nav-link px-3 nav-link-hover" href={item.link}>
                    <strong>{item.name.toUpperCase()}</strong>
                  </a>
                </li>
              );
            }
          })}
          <li className="nav-item ms-5">
            <a
              className="nav-link px-3 button-dl"
              href={cv}
              target="_blank"
              rel="noopener noreferrer"
            >
              {"Télécharger mon CV".toUpperCase()}
            </a>
          </li>
        </ul>
      </div>
    </nav>
  );
};

export default NavBar;
