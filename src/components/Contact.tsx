import { useEffect, useState } from "react";
import {
  LoadCanvasTemplate,
  loadCaptchaEnginge,
  validateCaptcha,
} from "react-simple-captcha";

/**
 * @file Contact.tsx
 * @description Contact component
 * @date 17/12/2022 - 15:09:20
 * @author Mathieu Piton
 *
 * @returns {JSX.Element}
 */
const Contact = (): JSX.Element => {
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [message, setMessage] = useState("");
  const [status, setStatus] = useState("Envoyer le message");
  const [captcha, setCaptcha] = useState("");

  // Chargement du captcha à l'initialisation du composant (8 = nombre de caractères)
  useEffect(() => {
    loadCaptchaEnginge(8);
  }, []);

  // Stockage des valeurs des champs dans les states
  const onChange = (e: { target: { name: string; value: string } }) => {
    const { name, value } = e.target;
    switch (name) {
      case "name":
        setName(value);
        break;
      case "email":
        setEmail(value);
        break;
      case "message":
        setMessage(value);
        break;
      case "captcha":
        setCaptcha(value);
        break;
      default:
        break;
    }
  };

  // Validation des champs du formulaire
  const validateFields = () => {
    let isValid = true;
    if (!name) {
      document.querySelector(".val-name")?.classList.add("alert-validate");
      isValid = false;
    } else {
      document.querySelector(".val-name")?.classList.remove("alert-validate");
    }
    if (!email) {
      document.querySelector(".val-email")?.classList.add("alert-validate");
      isValid = false;
    } else {
      document.querySelector(".val-email")?.classList.remove("alert-validate");
    }
    if (!message) {
      document.querySelector(".val-message")?.classList.add("alert-validate");
      isValid = false;
    } else {
      document
        .querySelector(".val-message")
        ?.classList.remove("alert-validate");
    }
    if (!captcha) {
      document.querySelector(".val-captcha")?.classList.add("alert-validate");
      isValid = false;
    } else {
      document
        .querySelector(".val-captcha")
        ?.classList.remove("alert-validate");
    }

    // Vérification de la validité de l'email
    if (email) {
      let re = /\S+@\S+\.\S+/;
      if (!re.test(email)) {
        document.querySelector(".val-email")?.classList.add("alert-validate");
        isValid = false;
      } else {
        document
          .querySelector(".val-email")
          ?.classList.remove("alert-validate");
      }
    }

    // Vérification de la validité du captcha
    if (captcha) {
      if (validateCaptcha(captcha) === false) {
        // Ajout d'un message d'erreur
        document
          .querySelector(".val-captcha")
          ?.setAttribute("data-validate", "Captcha invalide");
        document.querySelector(".val-captcha")?.classList.add("alert-validate");
        isValid = false;
        loadCaptchaEnginge(6);
      } else {
        document
          .querySelector(".val-captcha")
          ?.classList.remove("alert-validate");
      }
    }

    if (isValid) {
      return true;
    } else {
      return false;
    }
  };

  // Réinitialisation des champs du formulaire
  const resetFields = () => {
    setName("");
    setEmail("");
    setMessage("");
    setCaptcha("");
  };

  // Envoi du formulaire
  const submitForm = async (e: { preventDefault: () => void }) => {
    e.preventDefault();
    setStatus("Envoi en cours...");
    let details = {
      name,
      email,
      message,
    };
    let isValid = validateFields();
    // Si le formulaire est valide, on envoie les données
    if (isValid) {
      let response = await fetch(
        "https://getform.io/f/4ba8a4d2-d9e0-46ef-99a9-32856d110485",
        {
          method: "POST",
          headers: {
            "Content-Type": "application/json;charset=utf-8",
            Accept: "application/json",
          },
          body: JSON.stringify(details),
        }
      );
      await response.json();
      const validationMessage = document.querySelector(".is-valid");
      if (validationMessage) {
        validationMessage.classList.add("is-visible");
      }
      resetFields();
      setStatus("Envoyer le message");
    } else {
      resetFields();
      setStatus("Envoyer le message");
    }
  };

  return (
    <section className="container" id="contact">
      <div className="bg-contact2">
        <div className="container-contact2">
          <div className="wrap-contact2">
            <form className="contact2-form validate-form">
              <span className="contact2-form-title">Contactez moi</span>
              <div className="wrap-input2 is-valid">
                <i className="fa-solid fa-check"></i>{" "}
                <strong>Message envoyé !</strong> Je vous répondrai dans les
                plus brefs délais.
              </div>
              <div
                className="wrap-input2 validate-input val-name"
                data-validate="Veuillez entrer votre nom"
              >
                <input
                  className="input2"
                  type="text"
                  name="name"
                  value={name}
                  onChange={(e) => onChange(e)}
                />
                <span
                  className="focus-input2"
                  data-placeholder="NOM ET PRÉNOM"
                ></span>
              </div>
              <div
                className="wrap-input2 validate-input val-email"
                data-validate="Un email valide est requis: ex@abc.xyz"
              >
                <input
                  className="input2"
                  type="text"
                  value={email}
                  name="email"
                  onChange={(e) => onChange(e)}
                />
                <span className="focus-input2" data-placeholder="EMAIL"></span>
              </div>
              <div
                className="wrap-input2 validate-input val-message"
                data-validate="Un message est requis"
              >
                <textarea
                  className="input2"
                  name="message"
                  value={message}
                  onChange={(e) => onChange(e)}
                ></textarea>
                <span
                  className="focus-input2"
                  data-placeholder="MESSAGE"
                ></span>
              </div>
              <div className="col-12 mt-3">
                <LoadCanvasTemplate
                  reloadColor="#754ef9"
                  reloadText="Recharger le captcha"
                />
              </div>
              <input
                type="hidden"
                name="_gotcha"
                style={{ display: "none !important" }}
              />
              <div className="col-12 mt-3">
                <div
                  className="wrap-input2 validate-input val-captcha"
                  data-validate="Veuillez saisir le captcha"
                >
                  <input
                    name="captcha"
                    className="input2"
                    type="text"
                    onChange={(e) => onChange(e)}
                    value={captcha}
                  />
                  <span
                    className="focus-input2"
                    data-placeholder="CAPTCHA"
                  ></span>
                </div>
              </div>
              <div className="container-contact2-form-btn">
                <div className="wrap-contact2-form-btn">
                  <div className="contact2-form-bgbtn"></div>
                  <button
                    className="contact2-form-btn btn btn-primary"
                    onClick={(e: any) => submitForm(e)}
                  >
                    {status}
                  </button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Contact;
