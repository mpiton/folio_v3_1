const Arrow = () => {
  return (
    <button
      className="scroll-to-top-button"
      onClick={() => {
        window.scrollTo({ top: 0, behavior: "smooth" });
      }}
    >
      <i className="fa-solid fa-arrow-up"></i>
    </button>
  );
};

export default Arrow;
