import React, { useEffect } from "react";
import LightBox from "./LightBox";

const Feeds = ({ iframes }: any) => {
  const [image, setImage] = React.useState(
    "src/assets/no_image.png"
  ) as any;
  const [title, setTitle] = React.useState("") as any;
  const [description, setDescription] = React.useState("") as any;

  const transformDatetime = (date: string) => {
    const myDate = new Date(date);
    const options = {
      year: "numeric",
      month: "2-digit",
      day: "2-digit",
      hour: "2-digit",
      minute: "2-digit",
      hour12: false,
    } as any;
    const dateString = new Intl.DateTimeFormat("fr-FR", options).format(myDate);

    return dateString; // "15/01/2022 à 19:44"
  };

  useEffect(() => {
    async function fetchData() {
      try {
        const response = await fetch(iframes.link);
        const data = await response.text();

        const parser = new DOMParser();
        const doc = parser.parseFromString(data, "text/html");

        const title = doc
          .querySelector('meta[property="og:title"]')
          ?.getAttribute("content");
        const description = doc
          .querySelector('meta[property="og:description"]')
          ?.getAttribute("content");
        const image = doc
          .querySelector('meta[property="og:image"]')
          ?.getAttribute("content");

        setTitle(title);
        setDescription(description);
        setImage(image);
      } catch (error) {
        console.log("Impossible de récupérer le contenu de l'article");
      }
    }
    fetchData();
  }, []);

  return (
    <div className="container mt-4">
      <div className="card" style={{ minHeight: "660px" }}>
        <div className="card-body d-flex flex-column justify-content-center align-items-center">
          <LightBox src={image} alt={title}>
            <div
              style={{
                overflow: "hidden",
                height: "380px",
              }}
            >
              <img
                src={image}
                className="card-img-top"
                style={{
                  width: "100%",
                }}
                alt={title}
              />
            </div>
          </LightBox>

          <h5 className="card-title">{iframes.title}</h5>
          <p className="card-text">{description}</p>
        </div>
        <div className="card-footer d-flex justify-content-between align-items-center">
          <a
            href={iframes.link}
            target="_blank"
            rel="noreferrer"
            className="btn btn-primary"
          >
            Lire l'article
          </a>
          <small className="text-muted">
            {transformDatetime(iframes.pubDate)}
          </small>
        </div>
      </div>
    </div>
  );
};

export default Feeds;
