import img1 from "../assets/projects/aspects-maquette.jpg";
import img4 from "../assets/projects/ophra.png";
import img3 from "../assets/projects/projet-ifpa.png";
import img2 from "../assets/projects/rezo-4o4.png";
import LightBox from "./LightBox";

/**
 * @file Projects.tsx
 * @description Projects component
 * @date 17/12/2022 - 15:18:26
 * @author Mathieu Piton
 *
 * @returns {JSX.Element}
 */
const Projects = (): JSX.Element => {
  return (
    <section className="container-fluid col-12" id="projets">
      <div className="row align-items-center min-vh-100">
        {/* Projects */}
        <div className="col-md-12 mx-auto col-lg-12">
          <div className="min-vh-100">
            <div className="container pt-5">
              <div className="row">
                <div className="col-md-6 col-lg-5 mb-4 d-flex">
                  <div className="card d-flex flex-column justify-content-around">
                    <LightBox
                      src={img1}
                      alt="2016 - Intégration de cette maquette sous WordPress"
                    >
                      <div style={{ overflow: "hidden", height: "350px" }}>
                        <img
                          src={img1}
                          className="card-img-top"
                          style={{
                            width: "100%",
                          }}
                          alt="2016 - Intégration de cette maquette sous WordPress"
                        />
                      </div>
                    </LightBox>
                    <div className="d-flex justify-content-center align-items-center p-2 text-center">
                      <p
                        className="card-text fw-light"
                        style={{ fontSize: "0.8rem" }}
                      >
                        2016 - Intégration de cette maquette sous WordPress
                      </p>
                    </div>
                  </div>
                </div>

                {/* 2 */}

                <div className="col-md-6 col-lg-5 mb-4 d-flex">
                  <div className="card d-flex flex-column justify-content-around">
                    <LightBox
                      src={img2}
                      alt="2019 - Création d'un réseau social type Twitter sous ReactJS, Express et MongoDB"
                    >
                      <div style={{ overflow: "hidden", height: "350px" }}>
                        <img
                          src={img2}
                          className="card-img-top"
                          style={{
                            width: "100%",
                          }}
                          alt="2019 - Création d'un réseau social type Twitter sous ReactJS, Express et MongoDB"
                        />
                      </div>
                    </LightBox>
                    <div className="d-flex justify-content-center align-items-center p-2 text-center">
                      <p
                        className="card-text fw-light"
                        style={{ fontSize: "0.8rem" }}
                      >
                        2019 - Création d'un réseau social type Twitter sous
                        ReactJS, Express et MongoDB
                      </p>
                    </div>
                  </div>
                </div>

                {/* 3 */}
                <div className="col-md-6 col-lg-5 mb-4 d-flex">
                  <div className="card d-flex flex-column justify-content-around">
                    <LightBox
                      src={img3}
                      alt="2020 - Création d'un intranet pour une école sous la stack MERN (MongoDB, Express, ReactJS, NodeJS)"
                    >
                      <div style={{ overflow: "hidden", height: "350px" }}>
                        <img
                          src={img3}
                          className="card-img-top"
                          style={{
                            width: "100%",
                          }}
                          alt="2020 - Création d'un intranet pour une école sous la stack MERN (MongoDB, Express, ReactJS, NodeJS)"
                        />
                      </div>
                    </LightBox>
                    <div className="d-flex justify-content-center align-items-center p-2 text-center">
                      <p
                        className="card-text fw-light"
                        style={{ fontSize: "0.8rem" }}
                      >
                        2020 - Création d'un intranet pour une école sous la
                        stack MERN (MongoDB, Express, ReactJS, NodeJS)
                      </p>
                    </div>
                  </div>
                </div>

                {/* 4 */}

                <div className="col-md-6 col-lg-5 mb-4 d-flex">
                  <div className="card d-flex flex-column justify-content-around">
                    <LightBox
                      src={img4}
                      alt="2022 - Création de la partie frontend d'un intranet pour une banque sous ReactJS"
                    >
                      <div style={{ overflow: "hidden", height: "350px" }}>
                        <img
                          src={img4}
                          className="card-img-top"
                          style={{
                            width: "100%",
                          }}
                          alt="2022 - Création de la partie frontend d'un intranet pour une banque sous ReactJS"
                        />
                      </div>
                    </LightBox>
                    <div className="d-flex justify-content-center align-items-center p-2 text-center">
                      <p
                        className="card-text fw-light"
                        style={{ fontSize: "0.8rem" }}
                      >
                        2022 - Création de la partie frontend d'un intranet pour
                        une banque sous ReactJS
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Projects;
