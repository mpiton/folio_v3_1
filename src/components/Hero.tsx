import profile_pic from "../assets/profile.jpg";
import { author } from "../data/site.json";

import NavBar from "./NavBar";

/**
 * @file Hero.tsx
 * @description Hero component
 * @date 17/12/2022 - 15:13:31
 * @author Mathieu Piton
 *
 * @returns {JSX.Element}
 */
const Hero = (): JSX.Element => {
  return (
    <section className="container-fluid col-12">
      <div className="row align-items-center min-vh-100">
        {/* Hero left */}
        <div className="col-lg-5 text-center text-lg-start hero-left">
          <div className="min-vh-100 d-flex flex-column align-items-center justify-content-between">
            <div className="py-2">
              <h2 className="text-center">PORTFOLIO</h2>
            </div>
            {/* Circle Avatar */}
            <div className="my-image">
              <img
                src={profile_pic}
                alt="Avatar"
                className="img-fluid rounded-circle hero-avatar"
              />
            </div>
            {/*  Be curious  */}
            <div className="py-2">
              <h3 className="text-center">BE CURIOUS</h3>
            </div>
          </div>
        </div>
        {/*  Hero right */}
        <div className="col-md-10 mx-auto col-lg-7">
          <div className="min-vh-100">
            <NavBar />
            <div className="hero-right">
              <div className="hero-right-title">
                <h1>{author.toUpperCase()}</h1>
                <h1>{author.toUpperCase()}</h1>
              </div>
              <div className="hero-right-subtitle">
                <h2>Senior Full Stack Developer</h2>
              </div>
            </div>
            {/* Social Media */}
            <div className="hero-right-social">
              <p>SUIVEZ MOI SUR ➟</p>
              <div className="social-icons">
                <ul>
                  <li>
                    <a
                      href="https://twitter.com/MathieuPiton"
                      target="_blank"
                      rel="noreferrer"
                    >
                      <i className="fa-brands fa-square-twitter"></i>
                    </a>
                  </li>
                  <li>
                    <a
                      href="https://www.linkedin.com/in/mathieu-piton"
                      target="_blank"
                      rel="noreferrer"
                    >
                      <i className="fa-brands fa-linkedin"></i>
                    </a>
                  </li>
                  <li>
                    <a
                      href="https://github.com/mpiton"
                      target="_blank"
                      rel="noreferrer"
                    >
                      <i className="fa-brands fa-square-github"></i>
                    </a>
                  </li>
                  <li>
                    <a
                      href="https://gitlab.com/mpiton"
                      target="_blank"
                      rel="noreferrer"
                    >
                      <i className="fa-brands fa-square-gitlab"></i>
                    </a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Hero;
