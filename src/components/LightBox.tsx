import { useState } from "react";

const style = {
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
  position: "fixed",
  top: "0",
  left: "0",
  width: "100vw",
  height: "100vh",
  backgroundColor: "rgba(0,0,0,0.7)",
  zIndex: 100,
} as any;

/**
 * @description LightBox component
 * @file LightBox.tsx
 * @date 17/12/2022 - 15:14:27
 * @author Mathieu Piton
 *
 * @params {any} children
 * @params {string} src
 * @params {string} alt
 * @params {any} Wrapper
 * @params {number} zIndex
 * @returns {JSX.Element}
 */
const LightBox = ({
  children,
  src,
  alt,
  Wrapper = "div",
  zIndex = 100,
}: {
  children: any;
  src: string;
  alt: string;
  Wrapper?: any;
  zIndex?: number;
}): JSX.Element => {
  const [isOpen, setIsOpen] = useState(false);

  const toggleIsOpen = () => {
    setIsOpen(!isOpen);
  };

  return (
    <Wrapper onClick={toggleIsOpen}>
      {children}
      {isOpen ? (
        <div style={style}>
          <div
            onClick={toggleIsOpen}
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              backgroundColor: "rgba(0,0,0,0.7)",
              cursor: "pointer",
              zIndex,
            }}
          >
            <img src={src} alt={alt} style={{ maxWidth: "80%" }} />
          </div>
        </div>
      ) : null}
    </Wrapper>
  );
};

export default LightBox;
