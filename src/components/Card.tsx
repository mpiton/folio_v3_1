/**
 * @file Card.tsx
 * @description Card component.
 * @date 17/12/2022 - 15:12:36
 * @author Mathieu Piton
 *
 * @returns {JSX.Element}
 */
const Card = (): JSX.Element => {
  return (
    <section className="container-fluid col-12">
      <div className="row align-items-center min-vh-100">
        {/* Cards */}
        <div className="col-md-12 mx-auto col-lg-12">
          <div className="min-vh-100">
            <div className="row pt-10 d-flex flex-row align-items-center justify-content-evenly">
              <div className="card profile-card-1">
                <i className="fa fa-clock profile"></i>
                <div className="card-content">
                  <h2>
                    On Time
                    <small>
                      Je fais en sorte de respecter correctement les contraintes
                      de temps définies à l'avance avec le client.
                    </small>
                  </h2>
                </div>
              </div>
              <div className="card profile-card-1">
                <i className="fa-solid fa-hands-bubbles profile"></i>
                <div className="card-content">
                  <h2>
                    Clean Work
                    <small>
                      Mes projets clients comportent toujours un code propre
                      (indentation, commentaires, etc.) et une documentation
                      claire et complète.
                    </small>
                  </h2>
                </div>
              </div>
              <div className="card profile-card-1">
                <i className="fa-solid fa-user-check profile"></i>
                <div className="card-content">
                  <h2>
                    Client Satisfaction
                    <small>
                      Je m'assure que mes clients soient satisfaits de mon
                      travail et que leurs besoins soient satisfaits.
                    </small>
                  </h2>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Card;
