import axios from "axios";
import { useEffect, useState } from "react";
import Arrow from "../components/Arrow";
import Feeds from "../components/Feeds";
import NavBar from "../components/NavBar";
/**
 * @file Veille.tsx
 * @description Veille page
 * @version 1.0.0
 * @license MIT
 * @date 17/12/2022 - 15:30:49
 * @author Mathieu Piton
 *
 * @returns {JSX.Element}
 */
const Veille = (): JSX.Element => {
  const [showScrollButton, setShowScrollButton] = useState(false);
  const [feeds, setFeeds] = useState([]);

  const getFeeds = async (): Promise<any> => {
    const response = await axios.get("https://veille.4o4.fr/feeds", {
      headers: {
        "Content-Type": "application/json",
      },
    });
    const data = await response.data;
    setFeeds(data);
  };

  useEffect(() => {
    window.addEventListener("scroll", () => {
      if (window.scrollY > 400) {
        setShowScrollButton(true);
      } else {
        setShowScrollButton(false);
      }
    });
    return () => {
      getFeeds();
    };
  }, []);

  return (
    <section className="container-fluid col-12">
      {showScrollButton && <Arrow />}
      <div className="row align-items-center">
        <div className="col-lg-5 text-center text-lg-start"></div>
        <div className="col-md-10 mx-auto col-lg-7">
          <NavBar />
        </div>
      </div>
      <div className="row my-4">
        {feeds.map((feed: any) => (
          <div className="col-md-6 col-lg-4 mb-4 d-flex" key={feed._id}>
            <Feeds iframes={feed} />
          </div>
        ))}
      </div>
    </section>
  );
};

export default Veille;
