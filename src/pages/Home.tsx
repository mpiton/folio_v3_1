import { useEffect, useState } from "react";
import Arrow from "../components/Arrow";
import Card from "../components/Card";
import Contact from "../components/Contact";
import Hero from "../components/Hero";
import NavBar from "../components/NavBar";
import Projects from "../components/Projects";

/**
 * @file Home.tsx
 * @description Home page
 * @version 1.0.0
 * @license MIT
 * @date 17/12/2022 - 15:26:02
 * @author Mathieu Piton
 *
 * @returns {JSX.Element}
 */
const Home = (): JSX.Element => {
  const [showScrollButton, setShowScrollButton] = useState(false);
  console.log(import.meta.env.FORM_URL);

  useEffect(() => {
    window.addEventListener("scroll", () => {
      if (window.scrollY > 400) {
        setShowScrollButton(true);
      } else {
        setShowScrollButton(false);
      }
    });
    const anchor = window.location.hash.slice(1);
    if (anchor) {
      const anchorEl = document.getElementById(anchor);
      if (anchorEl) {
        anchorEl.scrollIntoView();
      }
    }
  }, []);
  return (
    <section className="container-main">
      {showScrollButton && <Arrow />}
      <Hero />
      <Card />
      <Projects />
      <Contact />
    </section>
  );
};

export default Home;
